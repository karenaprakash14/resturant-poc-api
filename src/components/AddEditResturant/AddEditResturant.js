import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { addEditResturant, getResturantDetail } from '../../services/resturant';
import Loader from '../loader';

const AddEditResturant = (
    {
        open,
        handleClickOpen,
        handleClose,
        resturantId,
        getNewData
    }
) => {
    const isAddRequest = !resturantId;
    // resturantId 
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        if (resturantId)
            getResturantDetail(resturantId)
                .then(res => {
                    if (res.data.status) {
                        const data = res.data.resturant;
                        setName(data.name);
                        setAddress(data.address);
                    }
                })
                .catch(err => {
                    if (err.response && err.response.data) {
                        alert(err.response.data.message);
                    }
                })
    }, [resturantId])
    const handleAddEditResturant = () => {
        if (!name) return alert('Name is required.');
        if (!address) return alert('address is required.');

        setIsLoading(true);
        addEditResturant({ id: resturantId, name, address })
            .then(res => {
                if (res.data.success === 1) {
                    setIsLoading(false);
                    alert(res.data.message);
                    getNewData()
                    handleClose();
                }
            })
            .catch(err => {
                setIsLoading(false);
                if (err.response && err.response.data) {
                    alert(err.response.data.message);
                }
            })
    }

    return (
        <div>
            <Dialog fullWidth open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    {isAddRequest ? 'Add New Resturant' : 'Edit Resturant'}
                </DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Name"
                        type="text"
                        fullWidth
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                    />
                </DialogContent>
                <DialogContent>
                    <TextField
                        margin="dense"
                        id="address"
                        label="Address"
                        type="text"
                        fullWidth
                        onChange={(e) => setAddress(e.target.value)}
                        value={address}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button disabled={isLoading} onClick={handleAddEditResturant} color="primary">
                        Submit
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default AddEditResturant;