import axios from 'axios';
const API = process.env.REACT_APP_API_URL; 

// get-users =>  token
export const getUsersList = (token) => new Promise((resolve,reject) => { 
    const headers = {
        'x-auth': token
    }
    axios.get(`${API}/get-users`,{headers})
    .then(res => {
        resolve(res);
    })
    .catch(err => reject(err))
})