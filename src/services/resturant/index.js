import axios from 'axios';
const API = process.env.REACT_APP_API_URL;

// get-users =>  token
export const getAllResturants = (_page, _perPage, _search) => {

    const userData = JSON.parse(localStorage.getItem('userData'));
    const headers = {
        'x-auth': userData.token
    }
    var requestData = {
        all: false,
        limit: _perPage,
        page: _page,
        search: _search
    };

    return axios.post(`${API}/get-resturants`, requestData, { headers });
};

// addEditResturant
export const addEditResturant = (requestData) => {
    const userData = JSON.parse(localStorage.getItem('userData'));
    const headers = {
        'x-auth': userData.token
    }
    return axios.post(`${API}/add-edit-resturant`, requestData, { headers });
};


// getResturantDetail
export const getResturantDetail = (resturantId) => {
    const userData = JSON.parse(localStorage.getItem('userData'));
    const headers = {
        'x-auth': userData.token
    }
    return axios.get(`${API}/get-resturant-detail/${resturantId}`, { headers });
};

// deleteResturant
export const deleteResturant = (resturantId) => {
    const userData = JSON.parse(localStorage.getItem('userData'));
    const headers = {
        'x-auth': userData.token
    }
    return axios.post(`${API}/delete-resturant/${resturantId}`, {}, { headers });
};