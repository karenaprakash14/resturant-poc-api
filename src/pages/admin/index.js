import React, { useState, useEffect, useCallback } from 'react';
import DataTable from "react-data-table-component";
import Title from '../../components/title';
import UsersList from '../../components/users-list';
import _ from 'lodash';
import { getUsersList } from '../../services/dashboard';
import Loader from '../../components/loader';
import { getAllResturants, deleteResturant } from '../../services/resturant';
import { Card, Container } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import './index.css';
import AddEditResturant from '../../components/AddEditResturant/AddEditResturant';

const Admin = (props) => {

    const userData = JSON.parse(localStorage.getItem('userData'));
    const [isLoading, setIsLoading] = useState(false);
    const [usersList, setUsersList] = useState([]);
    const [isAddRequest, setIsAddRequest] = useState(false);
    const [data, setData] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [dataForUpdate, setDataForUpdate] = useState('');
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [resturantId, setResturantId] = React.useState(null);


    useEffect(() => {
        // get users list  
        setIsLoading(true);
        getAllResturantsAction()
        getUsersList(userData.token) // token
            .then(res => {
                if (res.data.success === 1) {
                    setIsLoading(false);
                    setUsersList(res.data.users)
                }
            })
            .catch(err => {
                setIsLoading(false);
                if (err.response && err.response.data) {
                    alert(err.response.data.message);
                }
            })
    }, [])


    // Columns for data table 
    const columns = [
        {
            name: "Name",
            selector: "name",
            sortable: true,
            grow: 1,
            allowOverflow: true,
            cell: row => (
                <p title={row.name} className="text-truncate  mb-0">
                    {row.name}
                </p>
            )
        },
        {
            name: "Address",
            selector: "address",
            sortable: true,
            cell: row => (
                <p title={row.address} className="text-truncate  mb-0">
                    {row.address}
                </p>
            )
        }, {
            name: "Delete",
            center: true,
            button: true,
            width: '56px', // custom width for icon button
            cell: row => (
                <div onClick={() => handleDeleteResturant(row._id)} className="data-list-action-icon">
                    <DeleteIcon />
                </div>
            )
        }, {
            name: "Edit",
            center: true,
            button: true,
            width: '56px', // custom width for icon button
            cell: row => (
                <div onClick={() => handleClickOpen(row._id)} className="data-list-action-icon">
                    <EditIcon />
                </div>
            )
        },
        {
            name: "View",
            center: true,
            button: true,
            width: '56px', // custom width for icon button
            cell: row => (
                <div className="data-list-action-icon">
                    <VisibilityIcon />
                </div>
            )
        },
    ];

    // get all resturants 
    const getAllResturantsAction = async (_page, _perPage, _search) => {
        setIsLoading(true);
        getAllResturants(_page, _perPage, _search)
            .then(result => {
                setIsLoading(false);
                if (result && result.data.status) {
                    setData(result.data.resturants)
                }
            }).catch(err => {
                setIsLoading(false);
                alert('error while getting resturants')
            })

    };

    // Handle per page change
    const handlePerRowsChange = async (newPerPage, page) => {
        setPerPage(newPerPage);
        getAllResturantsAction(page, newPerPage);
    };

    // handle page change
    const handlePageChange = (_page) => {
        setPage(_page)
        getAllResturantsAction(_page, perPage);
    };

    // Filter
    const debouncedSearch = useCallback(
        _.debounce(value => getAllResturantsAction(page, perPage, value), 500),
        [],
    );

    const handleFilterTextChange = (e) => {
        const value = e.target.value;
        if (!value) {
            setResetPaginationToggle(!resetPaginationToggle);
            setFilterText('');
        }
        setFilterText(value);
        debouncedSearch(value);
    };

    //   AddEditResturant 

    const handleClickOpen = (resturantId = '') => {
        setResturantId(resturantId);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleDeleteResturant = (_resturantId) => {
        deleteResturant(_resturantId)
            .then(res => {
                if (res.data.status) {
                    alert(res.data.message);
                    getAllResturantsAction();
                }
            })
            .catch(err => {
                if (err.response && err.response.data) {
                    alert(err.response.data.message);
                }
            })
    };

    return (
        <>
            <Title>
                Admin
            </Title>
            {isLoading ? <div style={{ color: 'blue' }}><Loader /></div> : ''}
            <Container>
                <Card>
                    {/* Filter component */}
                    <div className='filter-component' >
                        <input id="search" className='filter' type="text" placeholder="Filter" aria-label="Search Input" value={filterText} onChange={e => handleFilterTextChange(e)} />
                        <div onClick={() => handleClickOpen('')} className="data-list-action-icon">
                            <AddCircleIcon />
                        </div>
                    </div>
                    <DataTable
                        columns={columns}
                        data={data}
                        progressPending={isLoading}
                        pagination
                        paginationServer
                        paginationTotalRows={totalRows}
                        onChangeRowsPerPage={handlePerRowsChange}
                        onChangePage={handlePageChange}
                        noHeader
                        className='custom-data-table'
                        // paginationResetDefaultPage={resetPaginationToggle}
                        noDataComponent={<div>Resturants Not Found</div>}
                    />
                </Card>
            </Container>
            {/* AddEditResturant */}
            <AddEditResturant
                open={open}
                setOpen={setOpen}
                handleClickOpen={handleClickOpen}
                handleClose={handleClose}
                resturantId={resturantId}
                getNewData={getAllResturantsAction}
            />
        </>
    )
}

export default Admin;