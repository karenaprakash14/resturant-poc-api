import React, { useEffect } from 'react';
import './index.style.css';

const Home = (props) => {

    useEffect(() => {
        const userData = JSON.parse(localStorage.getItem('userData'));
        if (userData && userData.role === 'regular-user') props.history.push('/editor');
        if (userData && userData.role === 'admin') props.history.push('/admin');
    }, [])

    return (
        <>
            <div id='welcome-div'>
                <h2>Resturant POC</h2>
                <h3>Welcome to resturant review app</h3>
            </div>
        </>
    )
}

export default Home;